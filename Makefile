CXX      = clang++ -stdlib=libc++ -lc++abi
CXXFLAGS = -Wall -Wextra -O2 -ftree-vectorize -march=native -pedantic -flto -fstack-protector-strong --param=ssp-buffer-size=4
LDFLAGS  = -s -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
TARGET   = lightweighttpd
SRCS     = $(wildcard *.cpp)
OBJS     = ${SRCS:%.cpp=%.o}

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -fPIE -pie -o $@ -pthread -lmagic

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -pthread -std=c++1z -fPIE $< -c -o $@

.PHONY: clean

clean:
	$(RM) $(TARGET) *.o
