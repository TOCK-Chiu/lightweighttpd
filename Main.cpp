/*

  Main.cpp

*/

#include <cstdlib>
#include <cstring>
#include <map>
#include <utility>
#include <chrono>
#include <thread>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include "GlobalVar.hpp"
#include "Utility.hpp"
#include "Worker.hpp"





void Terminate(int Signal){

	fprintf(stderr, "\n%s | Received signal %d. Asking workers to stop...\n", lightweighttpd::GetTimeString().c_str(), Signal);

	lightweighttpd::GlobalStatus.OnTerminate = true;
	lightweighttpd::GlobalStatus.WorkerCondVar.notify_all();

	if( lightweighttpd::GlobalStatus.ServerSocket != -1 )
		close( lightweighttpd::GlobalStatus.ServerSocket );

	}



int BindToPort(int Port, int QueueSize){

	if( Port >= 65536 || Port <= 0 ){

		fprintf(stderr, "%d is not a valid port number.\n", Port);
		exit(1);

		}

	sockaddr_in ServerAddr;
	int Socket = -1;

	memset(&ServerAddr, 0, sizeof(ServerAddr));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(Port);

	if( ( Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP) ) == -1
	 || bind(Socket, reinterpret_cast<sockaddr*>(&ServerAddr), sizeof(ServerAddr)) == -1
	 || listen(Socket, QueueSize) == -1 ){

		fprintf(stderr, "Failed to bind or listen to a port. Errno = %d (%s).\n", errno, strerror(errno));
		exit(1);

		}

	return Socket;

	}



void AcceptAndPend(int ServerSocket, std::map<std::chrono::high_resolution_clock::time_point, int>& PendingQueue, const std::string& TimeString){

	sockaddr_in ClientAddr;
	socklen_t   ClientAddrSize = sizeof(ClientAddr);
	int         ClientSocket;

	ClientSocket = accept(ServerSocket, reinterpret_cast<sockaddr*>(&ClientAddr), &ClientAddrSize);

	if( ClientSocket != -1 ){

		char Buffer[128];
		int OptVal = 1;

		if( setsockopt(ClientSocket, SOL_TCP, TCP_CORK, &OptVal, sizeof(OptVal)) == -1 )
			fprintf(stderr, "%s | setsockopt failed. Errno = %d (%s).\n", TimeString.c_str(), errno, strerror(errno));

		PendingQueue.emplace( std::chrono::high_resolution_clock::now(), ClientSocket );

		// Print log.
		if( inet_ntop(AF_INET, &(ClientAddr.sin_addr), Buffer, sizeof(Buffer)) == nullptr )
			fprintf(stderr, "%s | inet_ntop failed. Errno = %d (%s).\n", TimeString.c_str(), errno, strerror(errno));
		else
			fprintf(stderr, "%s | Request from %s. Pending queue size after enqueue = %zu.\n", TimeString.c_str(), Buffer, PendingQueue.size());

		}
	else if( ! lightweighttpd::GlobalStatus.OnTerminate )
		fprintf(stderr, "%s | Failed to accept connection. Errno = %d (%s).\n", TimeString.c_str(), errno, strerror(errno));

	}



int main(int ArgCount, const char* ArgVar[]){

	if( ArgCount != 3 && ArgCount != 4 ){

		fprintf(stderr, "Usage:\n");
		fprintf(stderr, "\"%s\" <port> <root> [worker_pool_size]\n", ArgVar[0]);
		fprintf(stderr, "\nworker_pool_size is %zu by default.\n", lightweighttpd::DefaultConfig.NumberOfWorker);
		exit(0);

		}

	size_t ListenBacklog = lightweighttpd::DefaultConfig.ListenBacklog;
	size_t WorkBacklog = lightweighttpd::DefaultConfig.WorkBacklog;
	size_t PendingBacklog = lightweighttpd::DefaultConfig.PendingBacklog;
	auto PendingTimeoutMillisecond = lightweighttpd::DefaultConfig.PendingTimeoutMillisecond;

	if( PendingBacklog + 1 >= FD_SETSIZE ){

		fprintf(stderr, "PendingBacklog too large. (FD_SETSIZE = %d).\n", FD_SETSIZE);
		exit(1);

		}

	if( chdir(ArgVar[2]) == -1 ){

		fprintf(stderr, "chdir failed. Errno = %d (%s).\n", errno, strerror(errno));
		exit(1);

		}

	sigset_t SignalSet;

	if( signal(SIGINT, Terminate) == SIG_ERR
	 || signal(SIGQUIT, Terminate) == SIG_ERR
	 || signal(SIGTERM, Terminate) == SIG_ERR
	 || signal(SIGPIPE, SIG_IGN) == SIG_ERR
	 || sigemptyset(&SignalSet) == -1
	 || sigaddset(&SignalSet, SIGINT) == -1
	 || sigaddset(&SignalSet, SIGQUIT) == -1
	 || sigaddset(&SignalSet, SIGTERM) == -1 ){

		fprintf(stderr, "Failed to set signal handler or set signal set. Errno = %d (%s).\n", errno, strerror(errno));
		exit(1);

		}

	int ServerSocket = lightweighttpd::GlobalStatus.ServerSocket = BindToPort(atoi(ArgVar[1]), ListenBacklog);
	std::vector<std::thread> WorkerPool;

	// Spawn worker.
	{
		size_t PoolSize = (ArgCount == 4) ? atoi(ArgVar[3]) : lightweighttpd::DefaultConfig.NumberOfWorker;

		for(size_t Index = 0; Index < PoolSize; Index++)
			WorkerPool.emplace_back(lightweighttpd::Worker, Index);

		}

	// Clients who hasn't sent any data yet sits here.
	// First stores the timepoint boss accepted it. Second stores file descriptor.
	std::map<std::chrono::high_resolution_clock::time_point, int> PendingQueue;

	fprintf(stderr, "%s | Finished initialization. Start serving...\n", lightweighttpd::GetTimeString().c_str());

	// Start to serve request.
	while( lightweighttpd::GlobalStatus.OnTerminate == false ){

		fd_set  FdSet;
		int     FdMax = ServerSocket;
		timeval Timeout;

		Timeout.tv_sec = PendingTimeoutMillisecond.count() / 1000;
		Timeout.tv_usec = (PendingTimeoutMillisecond.count() % 1000) * 1000;

		FD_ZERO(&FdSet);
		FD_SET(ServerSocket, &FdSet);

		for(auto& Client: PendingQueue){

			FdMax = std::max(FdMax, Client.second);
			FD_SET(Client.second, &FdSet);

			}

		int Result = select(FdMax+1, &FdSet, nullptr, nullptr, &Timeout);
		std::string TimeString = lightweighttpd::GetTimeString();

		if( Result == -1 && ! lightweighttpd::GlobalStatus.OnTerminate ){

			fprintf(stderr, "%s | select failed. Errno = %d (%s).\n", TimeString.c_str(), errno, strerror(errno));
			continue;

			}

		if( FD_ISSET(ServerSocket, &FdSet) )
			Result--;

		if( Result > 0 ){

			size_t ClientQueueSize = 0;

			// Block singal or there may be deadlock.
			if( pthread_sigmask(SIG_BLOCK, &SignalSet, NULL) == -1 )
				fprintf(stderr, "%s | Failed to block signal. Errno = %d (%s).\n", TimeString.c_str(), errno, strerror(errno));

			// Dispatch.
			std::unique_lock<std::mutex> Lock( lightweighttpd::GlobalStatus.ClientQueueMutex );

			lightweighttpd::GlobalStatus.BossCondVar.wait(Lock, [=](void) -> bool {
				return lightweighttpd::GlobalStatus.ClientQueue.size() < WorkBacklog
				    || lightweighttpd::GlobalStatus.OnTerminate;
				});

			auto It = PendingQueue.begin();

			while( It != PendingQueue.end() && Result > 0 && lightweighttpd::GlobalStatus.ClientQueue.size() < WorkBacklog ){

				if( FD_ISSET(It->second, &FdSet) ){

					lightweighttpd::GlobalStatus.ClientQueue.emplace_back( It->second );
					It = PendingQueue.erase( It );
					Result--;

					}
				else
					++It;

				}

			ClientQueueSize = lightweighttpd::GlobalStatus.ClientQueue.size();
			Lock.unlock();
			lightweighttpd::GlobalStatus.WorkerCondVar.notify_one();

			if( pthread_sigmask(SIG_UNBLOCK, &SignalSet, NULL) == -1 )
				fprintf(stderr, "%s | Failed to unblock signal. Errno = %d (%s).\n", TimeString.c_str(), errno, strerror(errno));

			fprintf(stderr, "%s | Pending queue size after dequeue = %zu. Work queue size after enqueue = %zu.\n", TimeString.c_str(), PendingQueue.size(), ClientQueueSize);

			}

		auto Now = std::chrono::high_resolution_clock::now();
		auto It = PendingQueue.begin();
		size_t NumberOfRemovedClient = 0;

		while( It != PendingQueue.end() && (Now - It->first) > PendingTimeoutMillisecond ){

			NumberOfRemovedClient++;
			close( It->second );
			It = PendingQueue.erase( It );

			}

		if( NumberOfRemovedClient > 0 )
			fprintf(stderr, "%s | Timeouted %zu sockets.\n", TimeString.c_str(), NumberOfRemovedClient);

		if( FD_ISSET(ServerSocket, &FdSet) && PendingQueue.size() < PendingBacklog )
			AcceptAndPend(ServerSocket, PendingQueue, TimeString);

		}

	for(auto& Worker: WorkerPool)
		Worker.join();

	fprintf(stderr, "%s | Main thread is shutting down...\n", lightweighttpd::GetTimeString().c_str());

	for(auto& Client: PendingQueue)
		close( Client.second );

	for(int Client: lightweighttpd::GlobalStatus.ClientQueue)
		close(Client);

	return 0;

	}
