/*

  GlobalVar.hpp

*/

#ifndef __LIGHTWEIGHTTPD__GLOBAL_VAR__HPP__
#define __LIGHTWEIGHTTPD__GLOBAL_VAR__HPP__

#include <cstddef>
#include <string>
#include <utility>
#include <functional>
#include <deque>
#include <unordered_map>
#include <chrono>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>





namespace lightweighttpd {

	typedef std::pair<dev_t, ino_t> file_id_t;

	typedef struct {

		size_t operator()(const file_id_t& FileId) const {

			return std::hash<dev_t>()(FileId.first) ^ std::hash<ino_t>()(FileId.second);

			}

		} FileIdHash;

	typedef struct {

		bool   OnTerminate = false;
		int    ServerSocket = -1;

		std::deque<int>         ClientQueue;
		std::mutex              ClientQueueMutex;
		std::condition_variable BossCondVar;
		std::condition_variable WorkerCondVar;

		// First of std::pair stores MIME type. Second stores m_time.
		std::unordered_map<file_id_t, std::pair<std::string, time_t>, FileIdHash> MimeCache;
		std::shared_mutex CacheRwlock;

		} GlobalVar;

	struct GlobalConst {

		size_t NumberOfWorker;
		size_t ListenBacklog;
		size_t PendingBacklog;
		size_t WorkBacklog;
		std::chrono::milliseconds PendingTimeoutMillisecond;

		GlobalConst():
			PendingTimeoutMillisecond( 15000 ){

			NumberOfWorker = 4;
			ListenBacklog = 512;
			PendingBacklog = 512;
			WorkBacklog = 512;

			}

		};

	extern GlobalVar GlobalStatus;
	extern const GlobalConst DefaultConfig;

	}





#endif

