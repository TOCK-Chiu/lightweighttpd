/*

  Utility.cpp

*/

#include <ctime>
#include <unistd.h>
#include <sys/sendfile.h>
#include "Utility.hpp"





// This function is very slow!
std::string lightweighttpd::GetMimeTypeOfFile(const char* File, magic_t MagicCookie){

	const char* MimeType = magic_file(MagicCookie, File);

	if( MimeType == nullptr )
		MimeType = "application/octet-stream";

	return MimeType;

	}



std::string lightweighttpd::GetTimeString(void){

	time_t Time = time(nullptr);
	tm LocalTime;

	char TimeStrBuffer[128];

	localtime_r(&Time, &LocalTime);
	strftime(TimeStrBuffer, sizeof(TimeStrBuffer), "%F %r", &LocalTime);

	return TimeStrBuffer;

	}



bool lightweighttpd::RepWrite(int Fd, const void* Buffer, size_t Size){

	ssize_t Result = 0;

	while( Size > 0 && (Result = write(Fd, Buffer, Size)) != -1 ){

		Buffer = reinterpret_cast<const char*>(Buffer) + Result;
		Size -= Result;

		}

	return (Result == -1) || (Size != 0);

	}



bool lightweighttpd::RepSendfile(int OutFd, int InFd, size_t Size){

	ssize_t Result = 0;

	while( Size > 0 && (Result = sendfile(OutFd, InFd, nullptr, Size)) != -1 )
		Size -= Result;

	return (Result == -1) || (Size != 0);

	}



bool lightweighttpd::RepRead(int Fd, std::vector<char>& Buffer){

	char InternalBuffer[1024];
	ssize_t Result = 0;

	while( (Result = read(Fd, InternalBuffer, sizeof(InternalBuffer))) > 0 )
		Buffer.insert(Buffer.end(), std::begin(InternalBuffer), std::end(InternalBuffer));

	return (Result == -1);

	}
