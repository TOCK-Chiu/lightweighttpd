/*

  Worker.cpp

*/

#include <cstdio>
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <magic.h>
#include "GlobalVar.hpp"
#include "Worker.hpp"
#include "Utility.hpp"
#include "Http.hpp"





namespace {

	// Wrapper to output message when operation failed.
	void ServeAssert(size_t WorkerID, bool Result, const char* Service){

		if( Result )
			fprintf(stderr, "%s | Worker %zu %s failed. Error = %d (%s).\n", lightweighttpd::GetTimeString().c_str(), WorkerID, Service, errno, strerror(errno));

		}

	// Main serve logic in worker.
	void ServeRequest(size_t WorkerID, int ClientSocket, std::string& FilePath, magic_t MagicCookie){

		// If the request is invalid, the function returns empty string.
		if( FilePath.empty() ){

			ServeAssert(WorkerID, lightweighttpd::Serve403(ClientSocket), "Serve403");
			return;

			}

		int         FileFd = -1;
		struct stat FileStat;

		// Failed to open the file or failed to check its status.
		if( (FileFd = open(FilePath.c_str(), O_RDONLY)) == -1
		 || fstat(FileFd, &FileStat) == -1 ){

			// The file doesn't exist.
			if( errno == ENOENT )
				ServeAssert(WorkerID, lightweighttpd::Serve404(ClientSocket), "Serve404");
			// Insufficient permission?
			else
				ServeAssert(WorkerID, lightweighttpd::Serve403(ClientSocket), "Serve403");

			}
		// If the requested object is a directory.
		else if( S_ISDIR(FileStat.st_mode) ){

			// No need to redirect. Serve the request.
			if( FilePath.back() == '/' )
				ServeAssert(WorkerID, lightweighttpd::ServeDirectoryOrIndexHtml(ClientSocket, FileFd, FilePath.c_str()), "ServeDirectoryOrIndexHtml");
			else
				// FIXME: can it be relative path?
				ServeAssert(WorkerID, lightweighttpd::Serve301(ClientSocket, (FilePath += '/').c_str()), "Serve301");

			}
		else{

			lightweighttpd::file_id_t FileId(FileStat.st_dev, FileStat.st_ino);
			std::string FileType;

			lightweighttpd::GlobalStatus.CacheRwlock.lock_shared();

			auto It = lightweighttpd::GlobalStatus.MimeCache.find( FileId );

			// If the MIME info is found and is never older than the m_time we found.
			if( It != lightweighttpd::GlobalStatus.MimeCache.end() && (It->second).second >= FileStat.st_mtime ){

				// Fetch from cache.
				FileType = (It->second).first;
				lightweighttpd::GlobalStatus.CacheRwlock.unlock_shared();

				}
			// If we can't found one or it's outdated.
			else{

				lightweighttpd::GlobalStatus.CacheRwlock.unlock_shared();

				FileType = lightweighttpd::GetMimeTypeOfFile(FilePath.c_str(), MagicCookie);

				// Before insert stuff into cache, acquire it exclusively.
				lightweighttpd::GlobalStatus.CacheRwlock.lock();

				// Query it again. Other worker may do the same thing once they couldn't found suitable MIME info either.
				It = lightweighttpd::GlobalStatus.MimeCache.find( FileId );

				// If other workers have inserted a suitable record, it's no need to do it again.
				// Otherwise, put this worker's found into the cache or overwrite an outdated one.
				if( It == lightweighttpd::GlobalStatus.MimeCache.end() || (It->second).second < FileStat.st_mtime )
					lightweighttpd::GlobalStatus.MimeCache[FileId] = std::pair<std::string, time_t>(FileType, FileStat.st_mtime);

				lightweighttpd::GlobalStatus.CacheRwlock.unlock();

				}

			ServeAssert(WorkerID, lightweighttpd::ServeFile(ClientSocket, FileFd, FileType.c_str(), FileStat.st_size), "ServeFile");

			}

		if( FileFd != -1 )
			close(FileFd);

		}

	}



void lightweighttpd::Worker(size_t WorkerID){

	magic_t MagicCookie;

	{
		sigset_t    SignalSet;
		std::string TimeString = lightweighttpd::GetTimeString();

		if( sigfillset(&SignalSet) == -1
		 || pthread_sigmask(SIG_SETMASK, &SignalSet, NULL) == -1 ){

			fprintf(stderr, "%s | Worker %zu failed to block signal. Errno = %d (%s).\n", TimeString.c_str(), WorkerID, errno, strerror(errno));
			return;

			}

		// Initialize libmagic.
		if( (MagicCookie = magic_open(MAGIC_MIME | MAGIC_SYMLINK)) == NULL ){

			fprintf(stderr, "%s | Worker %zu magic_open failed. Errno = %d (%s).\n", TimeString.c_str(), WorkerID, magic_errno(MagicCookie), magic_error(MagicCookie));

			return;

			}
		else if( magic_load(MagicCookie, nullptr) == -1 ){

			fprintf(stderr, "%s | Worker %zu magic_load failed. Errno = %d (%s).\n", TimeString.c_str(), WorkerID, magic_errno(MagicCookie), magic_error(MagicCookie));
			magic_close( MagicCookie );
			return;

			}

		fprintf(stderr, "%s | Worker %zu started. Start to serve...\n", TimeString.c_str(), WorkerID);

		}

	while( 1 ){

		int    ClientSocket = -1;
		size_t ClientQueueSize = 0;

		{
			std::unique_lock<std::mutex> Lock( lightweighttpd::GlobalStatus.ClientQueueMutex );

			lightweighttpd::GlobalStatus.WorkerCondVar.wait(Lock, [](void) -> bool {
				return lightweighttpd::GlobalStatus.OnTerminate || ! lightweighttpd::GlobalStatus.ClientQueue.empty();
				});

			if( lightweighttpd::GlobalStatus.OnTerminate )
				break;

			ClientSocket = lightweighttpd::GlobalStatus.ClientQueue.front();
			lightweighttpd::GlobalStatus.ClientQueue.pop_front();
			ClientQueueSize = lightweighttpd::GlobalStatus.ClientQueue.size();
			Lock.unlock();
			lightweighttpd::GlobalStatus.BossCondVar.notify_one();

			}

		fprintf(stderr, "%s | Worker %zu dequeue one request. Work queue size after dequeue = %zu.\n", lightweighttpd::GetTimeString().c_str(), WorkerID, ClientQueueSize);

		// FIXME: it doesn't read them all.
		char Buffer[4096];
		ssize_t RequestLength = read(ClientSocket, Buffer, sizeof(Buffer));

		// Zero indicates EOF. Ignore it then. It may be caused by timeout tho.
		if( RequestLength == -1 )
			fprintf(stderr, "%s | Worker %zu read failed. Error = %d (%s).\n", lightweighttpd::GetTimeString().c_str(), WorkerID, errno, strerror(errno));
		else if( RequestLength > 0 ){

			std::string FilePath = lightweighttpd::GetRequestFileName(Buffer, RequestLength);
			ServeRequest(WorkerID, ClientSocket, FilePath, MagicCookie);

			}

		close(ClientSocket);

		}

	fprintf(stderr, "%s | Worker %zu is shutting down...\n", lightweighttpd::GetTimeString().c_str(), WorkerID);
	magic_close( MagicCookie );
	return;

	}

