/*

  Http.cpp

*/

#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "Http.hpp"
#include "Utility.hpp"





std::string lightweighttpd::GetRequestFileName(const char* Request, size_t Length){

	std::string FilePath;

	if( Length >= 4 && strncmp(Request, "GET ", 4) == 0 ){

		std::string Name;

		Request += 4;
		Length -= 4;
		char Char = *Request;

		// Ignore leading spaces.
		while( Length > 0 && Char == ' ' )
			Char = *(++Request);

		// Only parse the first line after "GET" and before other shitty stuff.
		while( Length > 0 && Char != ' ' && Char != '\0' && Char != '?' && Char != '\r' && Char != '\n' ){

			// TODO: convert UTF-8 escape seq into raw UTF-8.
			if( Char == '/' ){

				if( Name == ".." || Name == "." )
					return std::string();

				if( ! Name.empty() ){

					FilePath += Name;
					FilePath += '/';
					Name.clear();

					}

				}
			else
				Name += Char;

			Char = *(++Request);
			Length--;

			}

		if( Name == ".." || Name == "." )
			return std::string();

		FilePath += Name;

		if( FilePath.empty() )
			FilePath = "./";

		}

	return FilePath;

	}



bool lightweighttpd::Serve403(int ClientSocket){

	const char* Str = "HTTP/1.1 403 Forbidden\r\n\r\n";

	return lightweighttpd::RepWrite(ClientSocket, Str, strlen(Str));

	}



bool lightweighttpd::Serve404(int ClientSocket){

	const char* Str = "HTTP/1.1 404 Not Found\r\n\r\n";

	return lightweighttpd::RepWrite(ClientSocket, Str, strlen(Str));

	}



bool lightweighttpd::Serve503(int ClientSocket){

	const char* Str = "HTTP/1.1 503 Service Unavailable\r\n\r\n";

	return lightweighttpd::RepWrite(ClientSocket, Str, strlen(Str));

	}



bool lightweighttpd::Serve200(int ClientSocket, size_t ContentSize, const char* ContentType){

	std::string Response = "HTTP/1.1 200 OK\r\nContent-Type: ";

	Response += ContentType;
	Response += "\r\nContent-Length: ";
	Response += std::to_string(ContentSize);
	Response += "\r\n\r\n";

	return lightweighttpd::RepWrite(ClientSocket, Response.c_str(), Response.size());

	}



bool lightweighttpd::Serve301(int ClientSocket, const char* NewLocation){

	std::string Response = "HTTP/1.1 301 Moved Permanently\r\nLocation: ";

	Response += NewLocation;
	Response += "\r\n\r\n";

	return lightweighttpd::RepWrite(ClientSocket, Response.c_str(), Response.size());

	}



bool lightweighttpd::ServeFileList(int ClientSocket, int DirFd, const char* Title){

	// closedir closes file descriptor associating fopendir.
	if( (DirFd = dup(DirFd)) == -1 ){

		fprintf(stderr, "%s | dup failed. Errno = %d (%s).\n", lightweighttpd::GetTimeString().c_str(), errno, strerror(errno));
		return lightweighttpd::Serve503(ClientSocket);

		}

	DIR* Dir = fdopendir(DirFd);

	if( Dir == nullptr ){

		close(DirFd);
		fprintf(stderr, "%s | fdopendir failed. Errno = %d (%s).\n", lightweighttpd::GetTimeString().c_str(), errno, strerror(errno));
		return lightweighttpd::Serve503(ClientSocket);

		}

	std::string Response = "<html><head><title>lightweighttpd</title></head><body><h2>";
	struct dirent* Entry;

	Response += Title;
	Response += "</h2><pre><ul>";

	while( (Entry = readdir(Dir)) != nullptr ){

		// TODO: html escape.
		std::string EntryName = Entry->d_name;

		if( Entry->d_type == DT_DIR )
			EntryName += '/';

		Response += "<li><a href=\"./";
		Response += EntryName;
		Response += "\">";
		Response += EntryName;
		Response+= "</a></li>";

		}

	closedir(Dir);
	Response += "</ul></pre></body></html>";

	return Serve200(ClientSocket, Response.size(), "text/html; charset=utf-8")
	    || lightweighttpd::RepWrite(ClientSocket, Response.c_str(), Response.size());

	}



bool lightweighttpd::ServeFile(int ClientSocket, int FileFd, const char* FileType, size_t FileSize){

	return lightweighttpd::Serve200(ClientSocket, FileSize, FileType)
	    || lightweighttpd::RepSendfile(ClientSocket, FileFd, FileSize);

	}



bool lightweighttpd::ServeDirectoryOrIndexHtml(int ClientSocket, int DirFd, const char* DirName){

	std::string IndexHtml = DirName;
	int         IndexHtmlFd;
	struct stat IndexHtmlStat;

	IndexHtml += "index.html";
	IndexHtmlFd = open(IndexHtml.c_str(), O_RDONLY);

	if( IndexHtmlFd == -1 ){

		if( errno != ENOENT )
			return Serve403(ClientSocket);
		// Not found.
		else
			return ServeFileList(ClientSocket, DirFd, DirName);

		}

	bool Result = 0;

	if( fstat(IndexHtmlFd, &IndexHtmlStat) == -1 )
		Result = Serve403(ClientSocket);
	else if( S_ISDIR(IndexHtmlStat.st_mode) )
		Result = ServeFileList(ClientSocket, DirFd, DirName);
	else
		Result = ServeFile(ClientSocket, IndexHtmlFd, "text/html; charset=utf-8", IndexHtmlStat.st_size);

	close(IndexHtmlFd);
	return Result;

	}
