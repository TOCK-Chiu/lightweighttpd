Introduction
====================
I've done all the basic requirement in my implementation, i.e.:
*   GET a static object
*   GET a directory

[The homework main page](http://people.cs.nctu.edu.tw/~chuang/courses/unixprog/resources/hw4_web/). The code is also hosted on [Bitbucket](https://bitbucket.org/TOCK-Chiu/lightweighttpd).

Design
====================
The implementation can be broken down into two parts: the dispatcher and the workers. The dispatcher accepts incoming connections and put them into its `pending queue`. It calls `select` to monitor the pending queue and moves connections which can be read without being blocking into a global `client queue`. `Pending queue` also records the timepoint which the dispatcher accepted it so that it can timeout connections if the corresponding client hadn't send any data. The workers are responsible to dequeue the request from `client queue` and serve the client. Each worker reads request, responses to it and closes the connection.

The `client queue` is protected by a global mutex. The dispatcher waits for one condition variable and the workers wait for another. The first is notified when a worker dequeue a request, i.e. it becomes not full, and the later is notified when the dispatcher enqueues a request, i.e. it becomes not empty.

The workers leverage `libmagic` to identify the MIME type of requested files. Each worker has its own `magic cookie` becasue it can't be shared among serveral threads simultaneously. There is also a global cache of MIME info because `libmagic` can be very slow for some file types. The MIME cache is protected by a readers-writer lock (std::shared\_mutex since C++17). The MIME cache uses the device number and inode number to identify a unique file. It also stores the mtime of cached file so an worker can tell whether the record is outdated or not.

The implementation keep running until it has received some signals, e.g. `SIGINT` and `SIGTERM`. The dispatcher is responsible for signal handling, as a result the workers block signals to prevent them from being interrupted and doing the dispatcher's job. The dispatcher sets an global variable `OnTerminate` in response to the received signal to notify the workers on termination. Each worker checks the variable as well when they tries to dequeue a request, and returns immediately if the variable is set. Please note that the dispatcher also blocks signal before it tries to acquire the mutex, or the workers can't acquire the mutex and result in deadlock.

Known Issue
================
There are some known issues that may be solved in future:
*   The file `simple.html` provided by teacher is NOT considered as a HTML file because it's not complete. Please complete it, i.e. add `<html>...</html>`.
*   The worker calls `write` or `sendfile` with blocking to response the request. If the connection is ridiculously slow, it can slows down the worker and be exploited.
*   The worker only calls `read` once with buffer size 4096 bytes. If the connection is slow and the request can't be delivered completely in time, the request may be rejected if the implementation considers it invalid.
*   It doesn't support percent-encoding.
*   It doesn't support Unicode in listing files in directories.
*   It assumes the default encoding is UTF-8. Something strange may happen if the assumption doesn't hold.
