/*

  Http.hpp

*/

#ifndef __LIGHTWEIGHTTPD__HTTP__HPP__
#define __LIGHTWEIGHTTPD__HTTP__HPP__

#include <utility>
#include <string>





namespace lightweighttpd {

	std::string GetRequestFileName(const char* Request, size_t Length);
	bool Serve403(int ClientSocket);
	bool Serve404(int ClientSocket);
	bool Serve503(int ClientSocket);
	bool Serve200(int ClientSocket, size_t ContentSize, const char* ContentType);
	bool Serve301(int ClientSocket, const char* NewLocation);
	bool ServeFileList(int ClientSocket, int DirFd, const char* Title);
	bool ServeFile(int ClientSocket, int FileFd, const char* FileType, size_t FileSize);
	bool ServeDirectoryOrIndexHtml(int ClientSocket, int DirFd, const char* DirName);

	}





#endif

