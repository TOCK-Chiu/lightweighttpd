/*

  Utility.hpp

*/

#ifndef __LIGHTWEIGHTTPD__UTILITY__HPP__
#define __LIGHTWEIGHTTPD__UTILITY__HPP__

#include <string>
#include <vector>
#include <magic.h>





namespace lightweighttpd {

	std::string GetMimeTypeOfFile(const char* File, magic_t MagicCookie);
	std::string GetTimeString(void);
	bool RepWrite(int Fd, const void* Buffer, size_t Size);
	bool RepSendfile(int OutFd, int InFd, size_t Size);
	bool RepRead(int Fd, std::vector<char>& Buffer);

	}





#endif

